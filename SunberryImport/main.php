<?php
$files = glob( getcwd() . '/*/*.php' );
$secondLevel = glob( getcwd() . '/*/*/*.php' );
$files = array_merge($files, $secondLevel);

foreach ( $files as $file )
    require_once( $file );

$cli = new Cli();
//Get global value as array('brand' => $brand, 'imageDirectory' => $directory, 'file'=> $file);
$globalValues = $cli->getGlobalValues();

Helper::createMagmiCsv(Helper::createProducts('var/import/' . $globalValues['file']));
GlobalHelper::writeToFile($GLOBALS['globalValues']);

//Prompt user whether to run step 2 - checkSunberryImages
$importName = GlobalHelper::getImportName($globalValues);
$run =  $cli->checkNextStep(2);
if($run === 'Yes'){
    $isMissing = $cli->runNextStep(2);
}

//Prompt user whether to run step 3 - copyAndCheckImages
if(isset($isMissing) && $isMissing != true){
    $run =  $cli->checkNextStep(3);
    if($run === 'Yes'){
        $isMissing = $cli->runNextStep(3);
        if($isMissing == true){
            echo 'Please check missing-' . $importName . '.txt';
        }
    }
}else{
    echo 'Please check missing-' . $importName . '.txt';
}


