<?php
/**
 * Created by PhpStorm.
 * User: msomm
 * Date: 9/12/2018
 * Time: 1:23 PM
 */
/**
 * This script does three functions.
 * 1.Copies all rx images found in CheckImagesHelper->imagesToCopy
 * 2.Checks all images in var/export/{import name}images-file.csv (name retrieved from global.txt) are found in
 *  the image directory - if any are missing outputs list of missing images - missing-{import name}images-file.csv
 * 3.Outputs file with correct extensions to /var/export/corrected-{Import name*}-images-file.csv
 */

include_once 'Helper/GlobalHelper.php';
include_once 'Helper/CheckImagesHelper.php';
include_once 'Cli/Cli.php';
include_once 'Cli/Scripts.php';

$missingFile = 'missing-' . GlobalHelper::getImportName(GlobalHelper::readGlobalValues()) . '.txt';

$cli = new Cli();

//Runs Scrips->runStep3()
$isMissing = $cli->runNextStep(3);
if($isMissing == true){
    echo 'Please check ' . $missingFile . "\n";
}