# Description
Outlined below are the necessary steps for data imports (new products, images etc.) for SunberryRX, these all work through a custom script that was developed by @Moshe.

------

# Configurations
*  All scripts run off sunberryrx.com (live site)
*  All Magmi imports are **first** run on test.sunberryrx.com(test site)
*  Script is located `sunberryrx.com/product-import/SunberryImport`
*  The script files is `main.php`
*  The import files are sent to us by SunberryRX, the required data is outlined [here](http://wiki.sunberryrx.com/2018/05/28/procedure-for-new-product-import/)
-----
# Usage
-----

##** Step 1. Run** `$ php main.php`

**The script will prompt you with the following 3 multiple choice questions:**
   
 1. Please choose a brand:

    This will list all the brands the script is configured to work with.

    *  0: Oakley
    *  1: Ray-Ban
    *  e: Exit


 2. Which directory are your images in? :

    This will list all directories in html/media/import/{Brand}-New-Product to choose from.
    -{brand} is the name of the brand you chose for "Please choose a brand:"
    
    * 0: `Oakley-New-Product/FLAK-2-0`
    * 1: `Oakley-New-Product/Frogskin-XS`
    * 2: `Oakley-New-Poduct/New-Oakley-Import-5-8-18`
    * 3: `Oakley-New-Product/New-Oakley-Import-6-19-18`
    * 4: `Oakley-New-Product/New-Oakley-Import-8-29-18`
    * e: Exit
    
  3. Which file are you running the script off? :
  
    This will list all files in `SunberryImport/var/import` to choose from
    
    * 0: `Oakley-Import-8-29-18v2.csv`
    * e: Exit
    
    Once the script has all the answers to the prompts, the script runs and oupupts 6 files to `SunberryImport/var/export` - All files are named `{Import name}-{filename}.csv`

    `-{Import name}` is taken from the name fo the images directory you chose for "Which directory are your images in:"
   
    `-{filename}` is the name of the file, explained below.
    
    The file names below are to illustrate the proper nameing convention of the files. The example used is for the Oakley Import 8-29-19
    
    **The 6 outputted files:**
    
    1. `New-Oakley-Import-8-29-18-file.csv` - The main Magmi import file with all new simple and convfigurable products.
    2. `New-Oakley-Import-8-29-18-configurable-file.csv` - The Magmi file with the existing configurable products (ie. if we are adding a new frame to an existing model).
    3. `New-Oakley-Import-8-29-18-images-file.csv` - The file with the new product images. This ile will be used in Step 3.
    4. `New-Oakley-Import-8-29-18-images-sunberry-file.csv`  A list of image file names. These images are (or need to be) uploaded by SunberryRX.
    5. `New-Oakley-Import-8-29-18-swatches-file.csv` - A list of swatches. These swatches are (or need to be) uploaded by SunberryRX.
    6. `New-Oakley-Import-8-29-18-links-file.csv` - A lise of links on test. sunberryrx for all new and updates products This willl be sent to SunberryRx for review.
    
**Step 1 is now complete. The script will then prompt you with the following question:**
    
Do you want to runt the next step: `checkSunberryImages.php` ?
    
* 0: Yes
* 1: No
* e: Exit
        
If yes is selected, the script will continue to Step 2. Run `checkSunberryImages.php` (see below). If no is selected, the script will stop here. You can continue to step 2 later  without having to repeat Step 1.    

-------------

##**Step 2. Run** `$ php checkSunberryImages.php`

This script checks the following files created by `main.php` to see if the images listed in the files exist in the specified directory (what you chose for "Which directory are your images in" in Step 1)



          *{Import name*}-images-sunberry-file.csv
          *{Import name*}-swatches-file.csv
    
If any of the images are not found in the specified directory, the script otputs a file with a list of the missing image names to `SunberrImport/missing-{Import name*}.txt`

**Once all the images are succesfully found, Step 2. is completed. The script will then prompt you with the following question:**

Do you want to run the next step, `copyAndCheckImages.php`

* 0: Yes
* 1: No
* e: Exit
-------

##**Step 3. Run** `$ php copyAndCheckImages.php`

1. The script copies all the rx images that get copied from other lens images.
2. The script checks for all images in `{Import name*}-images-file.csv`. This second setp is only there as a precaution. 
We are essentially redoing what we did in Step 2. Run `checkSunberryImages.php` to ensure that all images are there. Including the images we just copied.
If any of the images are missing, the script outputs a message to the terminal "Please check missing `-{Import name*}.txt`"
3. Outputs file with correct extensions to `SunberryImport/var/export/coorected-{Import name*}-images-files.csv`
    
------    

##**Step 4. Run Magmi Import**

**Important: Run in Order!**

1. Main Magmi File --> `{Import name*}-file.csv`
2. Configurable File --> `{Import name*}-configurable-file.csv`
3. Images File --> `{Import name*}-images-sunberry-file.csv`

------
##**Step 5. Re-index**

