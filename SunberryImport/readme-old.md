## Description
Outlined below are the necessary steps for data imports (new products, images etc) for SunberryRx, these all work through a custom script that was developed by @Moshe.

## Configurations

 - All scripts are ran off of sunberryrx.com (live).
   
 - All Magmi imports are **first** ran on test.sunberryrx.com (test site).
   
 - Script is located sunberryrx.com/product-import/SunberryImport/
   
 - The script file is main.php
  
 - The import files are sent to us by SunberryRx, the required data is outlined [here](http://wiki.sunberryrx.com/2018/05/28/procedure-for-new-product-import/)

##  Usage
 
### 1. Run Scripts:

````
$ ssh sunberry
$ cd sunberryrx.com/product-import/SunberryImport/
$ php main.php `<fiel with data from sunberry>` `<image directory>`
````

Example: 
	    
The dada in this directory images were uplaoded by SunberryRx into `media/import/Oakley-New-Product/New-Oakley-Import-7-30/`

        $ php main.php data.csv Oakley-New-Product/New-Oakley-Import-7-30



Outputs 6 files - All files are named -month-day-hour-min-filename.csv

Example for July 30 11:43  (The hour is the server hour)

The first two files are **Magmi** files:

 - `07-30-03-43-file.csv`  - Main Magmi file with all new simple and configurable products.

 - `07-30-03-43-configurable-file.csv` - Magmi  File with the configurables that
   already exist.

 - `07-30-03-43images-file.csv` - File with the new product images.  (this file will be used in step 3)     
 - `07-30-03-43images-sunberry-file.csv` List of image file names that we    need from Sunberry. 
 - `07-30-03-43-swatches-file.csv`   List of    necessary swatches. (Also to be supplied by sunberry).    
 -  `07-30-03-43-links-file.csv` List of links (on test.sunberryrx.com) for all new and updated products. 
****
### 2.  Create a Copy of the 18 Copied Product Images  
 - **For Oakley Rx Imports Only!**
 - This occurs only after client uploads images

Copy all rx images with the following commands:

        $ php copyOakleyLensImages.php `<directory>` (in media/import)
        		
Example: 
	    
The images were uplaoded by SunberryRx into `media/import/Oakley-New-Product/New-Oakley-Import-7-30/`

        $ php copyOakleyLensImages.php Oakley-New-Product/New-Oakley-Import-7-30
    		

### 3.  Check if all Images are in the Directory and Output a Correct List of Images:

Run
    
    $ php checkImagesSansExt.php


4 Arguments: 
1. File to check against (the image file from original script - `07-30-03-43images-file.csv`) relative to current directory.
2. Name of file to output with correct image extensions relative to current directory.
3. Directory where images are found (The root directory is `media/import`).
4. Name of file with images that are not found (regardless of extension).


    $ php checkImagesSansExt.php <imageFile> <newImageFile> <directoryOfImages> <missingFile>
		    

Example:


    $ php checkImagesSansExt.php product-import/SunberryImport/07-30-03-43images-file.csv correctImages.csv Oakley-New-Product/New-Oakley-Import-7-30 missingImages.txt



The script outputs 2 files:

 - a Magmi file with correct extensions.  In the example above it's
   named `correctImages.csv`
 - a list of missing images. In the example above it's named `Oakley-New-Product/New-Oakley-Import-7-30 missingImages.txt`

***		
### 4. Check if all Swatches are in the  Directory 
**Important: Do not update as we need specific extension!**
		
 - Run mogrify to set the correct extensions - as all swatches need to be `png`.
 
        $ mogrify -format png *.jpg
		
 - Run
 
        $ php checkImageExists.php

3 Arguments:
1. File to check against (the swatches file from original script -> `07-30-03-43-swatches-file.csv`) Relative to current directory.
2. Name of file with images that aren't found(regardless of extension).
3. Directory where images are found (The root directory is `media/import`).

		$ php checkImageExists.php <swatchesFile> <misssingFile> <directory>

Example:

-
		$ php checkImageExists.php product-import/SunberryImport/07-30-03-43-swatches-file.csv missingSwatches.txt Oakley-New-Product/New-Oakley-Import-7-30/swatches
****
The script outputs:
 - a file with a list of images that aren't found. In the example above `missingSwatches.txt` 
	
### 5. Run Magmi Import

>**Important: Run in order!**
 - Main Magmi file ->  07-30-03-43-file.csv
 - Configurable file ->  07-30-03-43-configurable-file.csv
 - Images file -> correctImages.csv (remade file from step 3)

****		
### 6. Re-index		