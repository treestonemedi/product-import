<?php
/**
 * Created by PhpStorm.
 * User: msomm
 * Date: 9/12/2018
 * Time: 10:23 AM
 */
/**
 * Checks that all images and swatches provided by client are in image directory.
 * The following two files are checked
 * 1.var/export/{import name}images-sunberry-file.csv(name retrieved from global.txt)
 * 2.var/export/{import name}swatches-file.csv(name retrieved from global.txt)
 *
 * Outputs list of missing images and swatches to SunberryImport/missing{import name}.txt
 */
include_once 'Helper/GlobalHelper.php';
include_once 'Helper/CheckImagesHelper.php';
include_once 'Cli/Cli.php';
include_once 'Cli/Scripts.php';

$missingFile = 'missing-' . GlobalHelper::getImportName(GlobalHelper::readGlobalValues()) . '.txt';
$cli = new Cli();

//Runs Scrips->runStep2()
$isMissingImages = $cli->runNextStep(2);

if(!$isMissingImages){
    $run = $cli->checkNextStep(3);
    if($run === 'Yes'){
        $isMissing = $cli->runNextStep(3);
        if($isMissing == true){
            echo 'Please check $missingFile' . "\n";
        }
    }
}else{
    echo 'Please check ' . $missingFile . "\n";
}