<?php
/**
 * Created by PhpStorm.
 * User: msomm
 * Date: 1/9/2018
 * Time: 11:44 AM
 */

class RayBanConfigurableProduct extends RayBanProduct
{
    public $magentoProduct;

    function __construct(RayBanData $productData, $lowestPrice)
    {
        parent::__construct($productData);
        $this->setMagentoProduct($productData);
        $this->populateConfigurable($productData);
        $this->getOldConfigurable($productData);
        $this->setLowestPrice($lowestPrice);
    }


    function createSku($productData){
        return strtoupper(Helper::replaceChars($productData['model code']));
    }

    function populateConfigurable($productData){
        $this->magmiData['type'] = 'configurable';
        $this->magmiData['ts_rx_service'] = $productData->data['rx service'];
        $this->magmiData['configurable_attributes'] = 'ts_rx_service,ts_frame_color,ts_polarized,'.
            'ts_mirror,ts_gradient,ts_lens_color,ts_frame_size';
        $this->magmiData['Vision Type:radio:0:0'] = 'Single Vision:fixed:0::0 | Progressive:fixed:0::99';
        $this->magmiData['visibility'] = 'catalog,search';
        $this->magmiData['category_ids'] = $this->getCategoryIds($productData);
        $this->magmiData['ts_sports'] = $productData->data['sports specific'];
        $this->magmiData['ts_product_type'] = 'Sunglasses';
        $this->magmiData['ts_sorting'] = $this->getSorting($productData->data['position']);
        $this->magmiData['ts_max_cylinder'] = $productData->data['max cylinder'];
        $this->magmiData['ts_prescription_range'] = $productData->data['prescription range'];
        $this->magmiData['ts_progressive_range'] = $productData->data['progressive range'];
        $this->removeValues(
            array(
                'cost','ts_frame_color','ts_color_code','ts_lens_color','ts_mirror','ts_transition',
                'ts_gradient','tax_class_id', 'ts_polarized','ts_frame_size','ts_upc','ts_temple_length',
                'ts_lens_width','ts_lens_height','ts_lens_properties','ts_model_number'
            )
        );
    }
    function getOldConfigurable($productData){
        if(strtolower($productData->data['configurable sku']) != ''){
            $this->magmiData['sku'] = $productData->data['configurable sku'];
            $this->magmiData['url_key'] = $productData->data['configurable sku'];

            $this->assignOldValues();
        }
    }
    private function removeValues(Array $values){
        foreach($values as $value){
            unset($this->magmiData[$value]);
        }
    }

    function setLowestPrice($lowestPrice){
        if($this->magentoProduct){
            if($this->magentoProduct->getPrice() < $lowestPrice){
                $lowestPrice = $this->magentoProduct->getPrice();
            }
        }
        $this->magmiData['price'] = $lowestPrice;
    }


    function getCategoryIds($productData){
        $ids = $this->getAllCategoryIds();
        $oldCategories = array();

        if($this->magentoProduct){
            $oldCategories = $this->magentoProduct->getCategoryIds();
        }
        

        $newCategories = array($ids['Default Category-Brands'],$ids['Brands-Ray Ban']);
        $gender = ucfirst(strtolower($this->magmiData["ts_gender"]));

        switch ($gender){
            case 'Woman':
            case 'Women':
                array_push($newCategories,  $ids['Default Category-Womens']);
                break;
            case 'Men':
            case 'Man':
                array_push($newCategories, $ids['Default Category-Mens']);
                break;
            case 'Unisex':
                array_push($newCategories, $ids['Default Category-Womens'],$ids['Default Category-Mens']);
                break;
        }


        if($productData->hasRx){
            array_push($newCategories,
                $ids['Default Category-Prescription Sunglasses'],
                $ids['Prescription Sunglasses-Ray Ban']
            );
            switch ($gender){
                case 'Woman':
                case 'Women':
                    array_push($newCategories,  $ids['Womens-Prescription Sunglasses']);
                    break;
                case 'Men':
                case 'Man':
                    array_push($newCategories, $ids['Mens-Prescription Sunglasses']);
                    break;
                case 'Unisex':
                    array_push($newCategories,
                        $ids['Womens-Prescription Sunglasses'],
                        $ids['Mens-Prescription Sunglasses']
                    );
                    break;
            }
        }
        if ($productData->isStock){
            array_push($newCategories, $ids['Default Category-Sunglasses'],$ids['Sunglasses-Ray Ban']);
            switch ($gender){
                case 'Woman':
                case 'Women':
                    array_push($newCategories, $ids['Womens-Sunglasses']);
                    break;
                case 'Men':
                case 'Man':
                    array_push($newCategories, $ids['Mens-Sunglasses']);
                    break;
                case 'Unisex':
                    array_push($newCategories, $ids['Womens-Sunglasses'],$ids['Mens-Sunglasses']);
                    break;
            }
        }
        if($productData->data['sports specific'] !== ''){
            $sportsCategories = explode(',' ,$productData->data['sports specific']);
        }

        if(isset($sportsCategories)){
            $newCategories[] = $ids['Default Category-Sports Specific'];
            foreach ($sportsCategories as $category){
                if($category != '') {
                    $newCategories[] = $ids['Sports Specific' . '-' . $category];
                }
            }
        }
        sort($newCategories);

        if($newCategories != $oldCategories){
            $newCategories = array_unique(array_merge($newCategories, $oldCategories));

        }
        return implode(',',$newCategories );
    }

    function setMagentoProduct($productData){
        $M = new MagentoConfigurable();
        if(strtolower($productData->data['configurable sku']) != ''){
            $this->magentoProduct =  $M->load($productData->data['configurable sku']);
        }else{
            $M->notExist($this->magmiData['sku']);
        }
    }

    public  function checkSimpleSkus($products){
        if($this->magentoProduct){
            $configurableSku = $this->magmiData['sku'];
            foreach ($products as $product){
                $simpleSku = substr($product->magmiData['sku'], 0, strlen($configurableSku));
                if($configurableSku != $simpleSku){
                    exit("Simple sku (".$product->magmiData['sku'].") must begin with configurable($configurableSku)") ;
                }
            }
        }

    }

    function getSimpleSkus(array $newSimples){
        $simpleSkus = $newSimples;
        if($this->magentoProduct){
            $childIds = Mage::getModel('catalog/product_type_configurable')
                ->getChildrenIds($this->magentoProduct->getId());
            foreach ($childIds[0] as $id){
                $simpleSkus[] =  Mage::getModel('catalog/product')->load($id)->getSku();
            }
        }
        $this->magmiData['simples_skus'] =  implode(',',$simpleSkus );
    }

    private function assignOldValues(){
        $dataList = array(
            'name','url_key','status','price','ts_lens_base','ts_bridge_size','ts_folding', 'ts_bestseller',
            'ts_model_name_description','ts_lens_material','ts_shape','ts_frame_type', 'ts_temple_material',
            'ts_front_material', 'ts_gender','ts_theme','ts_brand','ts_product_type', 'visibility','qty',
            'is_in_stock','manage_stock','Vision Type:radio:0:0','ts_rx_service'
        );
        $this->removeValues($dataList);


    }

    function getAllCategoryIds(){
        $categories = Mage::getModel('catalog/category')
            ->getCollection()
            ->addAttributeToSelect('*');
        $categoryList = array();
        foreach ($categories as $cat){
            $categoryList[$cat->getId()] = array('Name' => $cat->getName(), 'ParentId' => $cat->getParentId());
        }
        $categoryIds = array();
        foreach ($categoryList as $id => $category){
            $categoryIds[$categoryList[$category['ParentId']]['Name'] . '-' .$category['Name']] = $id;
        }
        return $categoryIds;
    }

    function getSorting($sort){
        return str_pad($sort, 6,'0',STR_PAD_LEFT);
    }

}