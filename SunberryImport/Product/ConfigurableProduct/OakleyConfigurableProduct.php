<?php
/*
 * Extends OakleyProduct which Extends product
 *
 * Inherited Properties from Product
 * $magmiData;
 * function populateProduct(Array $productData)
 * abstract protected function createSku($productData)
 *
 * Inherited Properties from OakleyProduct
 * function getLensProperties($productData)
 * function createSku($productData)
 * function getOakleyCategoryFilter($oakleyCategory, $oakleyCollection)
 */

class OakleyConfigurableProduct extends OakleyProduct
{
    public $magentoProduct;

    function __construct(OakleyData $productData, $lowestPrice)
    {
        parent::__construct($productData);
        $this->setMagentoProduct($productData);
        $this->populateConfigurable($productData);
        $this->getOldConfigurable($productData);
        $this->setLowestPrice($lowestPrice);
        $this->setGlassesData($productData);
    }


    function createSku($productData){
        return strtoupper(Helper::replaceChars($productData['model name description']));
    }

    function populateConfigurable($productData){
        $this->magmiData['type'] = 'configurable';
        $this->magmiData['ts_rx_service'] = $productData->data['rx service'];
        $this->magmiData['configurable_attributes'] = 'ts_rx_service,ts_frame_color,ts_polarized,ts_prizm,ts_transition,ts_lens_color,ts_frame_size';
        $this->magmiData['Vision Type:radio:0:0'] =
            (RxHelper::checkShield($productData->data['model name description']) ?
                'Single Vision:fixed:0::0' :
                'Single Vision:fixed:0::0 | Progressive:fixed:0::99' );
        $this->magmiData['visibility'] = 'catalog,search';
        $this->magmiData['category_ids'] = $this->getCategoryIds($productData);
        $this->magmiData['ts_product_type'] = $this->getProductType($productData->data);
        $this->magmiData['ts_sorting'] = $this->getSorting($productData->data['sort order']);
        $this->magmiData['ts_oakley_collection'] = $this->getCollection($productData);
        $this->magmiData['ts_sports'] = $productData->data['sports specific'];
        $this->magmiData['ts_max_cylinder'] = $productData->data['max cylinder'];
        $this->magmiData['ts_prescription_range'] = $productData->data['prescription range'];
        $this->magmiData['ts_progressive_range'] = $productData->data['progressive range'];
        $this->magmiData['manage_stock'] = 0;
        $this->removeValues(
            array(
            'cost','ts_frame_color','ts_color_code','ts_rx_service','ts_lens_color','ts_prizm','ts_transition','ts_iridium','tax_class_id',
            'ts_polarized','ts_frame_size','ts_upc','ts_temple_length','ts_lens_width','ts_lens_height','ts_lens_properties'
            ,'qty','ts_geofit','is_in_stock','qty','ts_model_number'
            )
        );
    }
    function getOldConfigurable($productData){
        if(strtolower($productData->data['configurable sku']) != ''){
            $this->magmiData['sku'] = $productData->data['configurable sku'];
            $this->magmiData['url_key'] = $productData->data['configurable sku'];
            $this->assignOldValues();
        }
    }
    function getSimpleSkus(array $newSimples){
        $simpleSkus = $newSimples;
        if($this->magentoProduct){
            $childIds = Mage::getModel('catalog/product_type_configurable')
                ->getChildrenIds($this->magentoProduct->getId());
            foreach ($childIds[0] as $id){
                $simpleSkus[] =  Mage::getModel('catalog/product')->load($id)->getSku();
            }
        }
        $this->magmiData['simples_skus'] =  implode(',',$simpleSkus );
    }
    private function removeValues(Array $values){
        foreach($values as $value){
            unset($this->magmiData[$value]);
        }
    }
    private function assignOldValues(){
        $dataList = array(
            'name','url_key','status','price','ts_lense_base','ts_bridge_size','ts_folding',
            'ts_bestseller','ts_model_name_description', 'ts_lens_material','ts_shape','ts_frame_type','ts_temple_material','ts_front_material',
            'ts_gender','ts_theme','ts_brand','ts_product_type','description', 'short_description','visibility','manage_stock',
            'Vision Type:radio:0:0','ts_sorting','ts_oakley_collection'
        );

        $this->removeValues($dataList);
    }

    function setLowestPrice($lowestPrice){
            if($this->magentoProduct){
               if($this->magentoProduct->getPrice() < $lowestPrice){
                   $lowestPrice = $this->magentoProduct->getPrice();
               }
            }
        $this->magmiData['price'] = $lowestPrice;
    }
    function getProductType($data){
        $tsProductType = '';
        switch($data['category code']){
            case 'OO':
            case 'OJ':
                $tsProductType = 'Sunglasses';
                break;

            case 'OX':
            case 'OY':
                $tsProductType = 'Glasses';
                break;
        }
        return  $tsProductType;
    }

    function getCategoryIds($productData){
        $ids =  $this->getAllCategoryIds();
        $oldCategories = array();

        if($this->magentoProduct){
            $oldCategories = $this->magentoProduct->getCategoryIds();;
        }

        $newCategories = array($ids['Default Category-Brands'],$ids['Brands-Oakley']);
        $gender = ucfirst(strtolower($this->magmiData["ts_gender"]));

        if($productData->data['category code'] == 'OO' || $productData->data['category code'] == 'OJ'){
            if($productData->hasRx && $productData->isStock){
                switch ($gender){
                    case 'Woman':
                    case 'Women':
                        array_push($newCategories,
                            $ids['Default Category-Sunglasses'],
                            $ids['Sunglasses-Oakley'],
                            $ids['Default Category-Prescription Sunglasses'],
                            $ids['Prescription Sunglasses-Oakley'],
                            $ids['Default Category-Womens'],
                            $ids['Womens-Sunglasses'],
                            $ids['Womens-Prescription Sunglasses']
                        );
                        break;
                    case 'Men':
                    case 'Man':
                        array_push($newCategories,
                            $ids['Default Category-Sunglasses'],
                            $ids['Sunglasses-Oakley'],
                            $ids['Default Category-Prescription Sunglasses'],
                            $ids['Prescription Sunglasses-Oakley'],
                            $ids['Default Category-Mens'],
                            $ids['Mens-Sunglasses'],
                            $ids['Mens-Prescription Sunglasses']
                        );
                        break;
                    case 'Unisex':
                        array_push($newCategories,
                            $ids['Default Category-Sunglasses'],
                            $ids['Sunglasses-Oakley'],
                            $ids['Default Category-Prescription Sunglasses'],
                            $ids['Prescription Sunglasses-Oakley'],
                            $ids['Default Category-Womens'],
                            $ids['Womens-Sunglasses'],
                            $ids['Womens-Prescription Sunglasses'],
                            $ids['Default Category-Mens'],
                            $ids['Mens-Sunglasses'],
                            $ids['Mens-Prescription Sunglasses']                        );
                        break;
                }
            }elseif($productData->hasRx && !$productData->isStock){
                switch ($gender){
                    case 'Woman':
                    case 'Women':
                        array_push($newCategories,
                            $ids['Default Category-Prescription Sunglasses'],
                            $ids['Prescription Sunglasses-Oakley'],
                            $ids['Default Category-Womens'],
                            $ids['Womens-Prescription Sunglasses']
                        );
                        break;
                    case 'Men':
                    case 'Man':
                        array_push($newCategories,
                            $ids['Default Category-Prescription Sunglasses'],
                            $ids['Prescription Sunglasses-Oakley'],
                            $ids['Default Category-Mens'],
                            $ids['Mens-Prescription Sunglasses']
                        );
                        break;
                    case 'Unisex':
                        array_push($newCategories,
                            $ids['Default Category-Prescription Sunglasses'],
                            $ids['Prescription Sunglasses-Oakley'],
                            $ids['Default Category-Womens'],
                            $ids['Womens-Prescription Sunglasses'],
                            $ids['Default Category-Mens'],
                            $ids['Mens-Prescription Sunglasses']
                        );
                        break;
                }
            }elseif(!$productData->hasRx && $productData->isStock){
                switch ($gender){
                    case 'Woman':
                    case 'Women':
                        array_push($newCategories,
                            $ids['Default Category-Sunglasses'],
                            $ids['Sunglasses-Oakley'],
                            $ids['Default Category-Womens'],
                            $ids['Womens-Sunglasses']
                        );
                        break;
                    case 'Men':
                    case 'Man':
                        array_push($newCategories,
                            $ids['Default Category-Sunglasses'],
                            $ids['Sunglasses-Oakley'],
                            $ids['Default Category-Mens'],
                            $ids['Mens-Sunglasses']
                        );
                        break;
                    case 'Unisex':
                        array_push($newCategories,
                            $ids['Default Category-Sunglasses'],
                            $ids['Sunglasses-Oakley'],
                            $ids['Default Category-Womens'],
                            $ids['Womens-Sunglasses'],
                            $ids['Default Category-Mens'],
                            $ids['Mens-Sunglasses']
                        );
                        break;
                }
            }

        }elseif($productData->data['category code'] == 'OX' || $productData->data['category code'] == 'OY'){ //glasses
            switch ($gender){
                case 'Woman':
                case 'Women':
                    array_push($newCategories,
                        $ids['Default Category-Eyeglasses'],
                        $ids['Default Category-Womens'],
                        $ids['Womens-Eyeglasses']
                    );
                    break;
                case 'Men':
                case 'Man':
                    array_push($newCategories,
                        $ids['Default Category-Eyeglasses'],
                        $ids['Default Category-Mens'],
                        $ids['Mens-Eyeglasses']
                    );
                    break;
                case 'Unisex':
                    array_push($newCategories,
                        $ids['Default Category-Eyeglasses'],
                        $ids['Default Category-Womens'],
                        $ids['Womens-Eyeglasses'],
                        $ids['Default Category-Mens'],
                        $ids['Mens-Eyeglasses']
                    );
                    break;

            }
        }

        if($productData->data['category code'] == 'OJ' || $productData->data['category code'] == 'OY'){
            array_push($newCategories, $ids['Default Category-Youth']);
        }

        //Add sports categories

        if($productData->data['sports specific'] !== ''){
            $sportsCategories = explode(',' ,$productData->data['sports specific']);
        }

        if(isset($sportsCategories)){
            $newCategories[] = $ids['Default Category-Sports Specific'];
            foreach ($sportsCategories as $category){
                if($category != ''){
                    $newCategories[] = $ids['Sports Specific'  . '-'. $category];
                }

            }
        }

        sort($newCategories);
        if($newCategories != $oldCategories){
            $newCategories = array_unique(array_merge($newCategories, $oldCategories));

        }
        return implode(',',$newCategories );
    }

    function setMagentoProduct($productData){
        $M = new MagentoConfigurable();
        if(strtolower($productData->data['configurable sku']) != ''){
            $this->magentoProduct =  $M->load($productData->data['configurable sku']);
        }else{
            $M->notExist($this->magmiData['sku']);
        }
    }

    public  function checkSimpleSkus($products){
        if($this->magentoProduct){
            $configurableSku = $this->magmiData['sku'];
            foreach ($products as $product){
                $simpleSku = substr($product->magmiData['sku'], 0, strlen($configurableSku));
                if($configurableSku != $simpleSku){
                    exit("Simple sku (".$product->magmiData['sku'].") must begin with configurable($configurableSku)") ;
                }
            }
        }
    }

    public function getCollection($productData){
        $collection = '';
        if($productData->data['oakley category'] != ''){
            $collection .= $productData->data['oakley category'];
        }

        if(strtolower($productData->data['bestseller']) == 'yes'){
            if($collection != ''){
                $collection .= ',OAKLEY BESTSELLER';
            }else {
                $collection .= 'OAKLEY BESTSELLER';
            }
        }

        if(strtolower($productData->data['new product']) == 'yes'){
            if($collection != ''){
                $collection .= ',NEW PRODUCT';
            }else {
                $collection .= 'NEW PRODUCT';
            }
        }

        return $collection;
    }


    function setGlassesData($data){
        switch($data->data['category code']){
            case 'OX':
            case 'OY':
            $this->removeValues(array('Vision Type:radio:0:0'));
            $this->magmiData['ts_minimum_sv_pd'] = $data->data['minimum single vision pd'];
            $this->magmiData['ts_cylinder'] = $data->data['cylinder'];
            $this->magmiData['Lens Type:radio:0:0']
                = 'Clear:fixed:184::0 | Transition Grey:fixed:310::2 | Transition Brown:fixed:310::3';
            $this->magmiData['configurable_attributes'] = 'ts_frame_color,ts_frame_size';
                break;
        }
    }

    function getAllCategoryIds(){
        $categories = Mage::getModel('catalog/category')
            ->getCollection()
            ->addAttributeToSelect('*');
        $categoryList = array();
        foreach ($categories as $cat){
            $categoryList[$cat->getId()] = array('Name' => $cat->getName(), 'ParentId' => $cat->getParentId());
        }
        $categoryIds = array();
        foreach ($categoryList as $id => $category){
            $categoryIds[$categoryList[$category['ParentId']]['Name'] . '-' .$category['Name']] = $id;
        }
        return $categoryIds;
    }
    function getSorting($sort){
        return str_pad($sort, 6,'0',STR_PAD_LEFT);
    }

}