<?php
/*
 * Extends product
 *
 * Inherited Properties
 * $magmiData;
 * function populateProduct(Array $productData)
 * abstract protected function createSku($productData)
 * abstract protected function getLensProperties($productData)
 * abstract protected function addLensProperties($productData);
 */
require_once( 'Product.php' );

class OakleyProduct extends Product
{

    function __construct(OakleyData $productData){
        parent::__construct($productData);
        $this->addLensProperties($productData);
        $this->magmiData['tax_class_id'] = 'Stock Products';

       // $this->setGlassesData($productData);
    }


    //Put together list of lens properties
    function getLensProperties($productData){
        $lensProperties = '';
        if(strtolower( $productData->data['prizm'])  == 'yes'){
            $lensProperties .= 'Prizm, ';
        }
        if(strtolower($productData->data['transition']) == 'yes'){
            $lensProperties .= 'Transition, ';
        }
        if(strtolower($productData->data['iridium']) == 'yes'){
            $lensProperties .= 'Iridium, ';
        }
        if(strtolower($productData->data['polarized']) == 'yes'){
            $lensProperties .= 'Polarized  ';
        }
        if(!empty($lensProperties)){
            return substr( $lensProperties, 0, -2);
        }
        return $lensProperties;
    }

    function addLensProperties($productData){
        $this->magmiData['ts_prizm'] = $productData->data['prizm'];
        $this->magmiData['ts_transition'] = $productData->data['transition'];
        $this->magmiData['ts_iridium'] = $productData->data['iridium'];
        $this->magmiData['ts_polarized'] = $productData->data['polarized'];
        $this->magmiData['ts_lens_properties'] = $this->getLensProperties($productData);
    }

    function createSku($productData){
        $sku = $productData['model name description'] . "-" .
               $productData['color code'] . "-" .
               $productData['frame size'];
        return strtoupper(Helper::replaceChars($sku));
    }

    function setGlassesData($data){
        switch($data->data['category code']){
            case 'OX':
            case 'OY':
            $this->magmiData['tax_class_id'] = 'none';
            $this->magmiData['ts_minimum_sv_pd'] = $data->data['minimum single vision pd'];
            $this->magmiData['ts_cylinder'] = $data->data['cylinder'];
            $this->removeValues(array('ts_prizm','ts_transition', 'ts_iridium','ts_polarized','ts_lens_properties' ));
            return;
                break;
        }
    }

    private function removeValues(Array $values){
        foreach($values as $value){
            unset($this->magmiData[$value]);
        }
    }

}