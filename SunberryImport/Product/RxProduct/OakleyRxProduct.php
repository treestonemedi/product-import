<?php
/*
 * Extends RxProduct which extends product
 *
 * Inherited Properties from Product
 * public $magmiData;
 * function populateProduct(Array $productData)
 * abstract  function createSku($productData)
 * abstract protected function addLensProperties($productData);
 *
 * Inherited properties from RxProduct
 *
 * abstract function getLensProperties($lensData);
 */
require_once('RxProduct.php');
class OakleyRxProduct extends RxProduct
{
    function __construct(ProductData $productData,Product $product, array $rxData, $optionNumber)
    {
        parent::__construct($productData,$product, $rxData, $optionNumber);
        $this->magmiData['ts_progressive_price'] =  $rxData['ts_progressive_price'] - $rxData['price'];
    }

    function createSku($product){
        return Helper::replaceChars(strtoupper($this->magmiData['name'] . "-" .
            $this->magmiData['ts_frame_color'] . "-" .
            $this->magmiData['ts_frame_size']));
    }

    function getLensProperties($rxData){
        $lensProperties = '';
        if(strtolower( $rxData['ts_prizm'])  == 'yes'){
            $lensProperties .= 'Prizm, ';
        }
        if(strtolower($rxData['ts_transition']) == 'yes'){
            $lensProperties .= 'Transition, ';
        }
        if(strtolower($rxData['ts_iridium']) == 'yes'){
            $lensProperties .= 'Iridium, ';
        }
        if(strtolower($rxData['ts_polarized']) == 'yes'){
            $lensProperties .= 'Polarized  ';
        }
        if(!empty($lensProperties)){
            return substr( $lensProperties, 0, -2);
        }
        return $lensProperties;
    }
    function addLensProperties($rxData)
    {
        $this->magmiData['ts_prizm'] = $rxData['ts_prizm'];
        $this->magmiData['ts_transition'] = $rxData['ts_transition'];
        $this->magmiData['ts_iridium'] = $rxData['ts_iridium'];
        $this->magmiData['ts_polarized'] = $rxData['ts_polarized'];
    }
    function disableOptions($option){
        if(in_array($option,array(9, 26))){
            $this->magmiData['status'] = 'disabled';
        }
    }
}