<?php
/*
 * Extends product
 *
 * Inherited Properties
 * public $magmiData;
 * function populateProduct(Array $productData)
 * abstract protected function createSku($productData)
 * abstract protected function addLensProperties($productData);
 */
require_once(dirname(__FILE__) . '/../Product.php');

abstract class RxProduct extends Product
{

    function __construct(ProductData $productData,Product $product,Array $rxData, $optionNumber){
        parent::__construct($productData);
            $sku = $this->createSku($productData->data) . "-$optionNumber-RX";
            $this->magmiData['sku'] = $sku;
            $this->magmiData['url_key'] = $sku;
            $this->magmiData['ts_rx_service'] = 'YES';
            $this->magmiData['ts_lens_color'] = $rxData['ts_lens_color'];
            $this->magmiData['price'] = $rxData['price'] + $productData->data['frame price'];
            $this->magmiData['ts_rx_frame_cost'] = $productData->data['rx frame cost'];
            $this->magmiData['ts_rx_lens_cost'] = $rxData['ts_rx_lens_cost'];
            $this->magmiData['cost'] = $rxData['ts_rx_lens_cost'] + $productData->data['rx frame cost'];
            $this->magmiData['ts_lens_properties'] = $this->getLensProperties($rxData);
            $this->magmiData['use_config_manage_stock'] = '0';
            $this->magmiData['manage_stock'] = '0';
            $this->removeValues(array('ts_color_code','ts_upc','inventory_qty'));
            $this->addLensProperties($rxData);

    }

    abstract protected function getLensProperties($rxData);
    abstract protected function addLensProperties($rxData);
    private function removeValues(Array $values){
        foreach($values as $value){
            unset($this->magmiData[$value]);
        }
    }
}
