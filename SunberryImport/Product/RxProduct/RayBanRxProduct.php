<?php
/*
 * Extends RxProduct which extends product
 *
 * Inherited Properties from Product
 * public $magmiData;
 * function populateProduct(Array $productData)
 * abstract  function createSku($productData)
 * abstract protected function addLensProperties($productData);
 *
 * Inherited properties from RxProduct
 *
 * abstract function getLensProperties($lensData);
 */
require_once('RxProduct.php');
class RayBanRxProduct extends RxProduct
{
    function __construct(ProductData $productData,Product $product, array $rxData, $optionNumber)
    {
        parent::__construct($productData,$product, $rxData, $optionNumber);
        $this->magmiData['ts_progressive_price'] =  149.99;
        $this->magmiData['name'] = $productData->data['model code'];
    }

    function createSku($productData){
        return Helper::replaceChars(strtoupper($productData['model code'] . "-" .
            $productData['frame color'] . "-" .
            $productData['frame size']));
    }

    function getLensProperties($rxData)
    {

        $lensProperties = '';
        if(strtolower( $rxData['ts_gradient'])  == 'yes'){
            $lensProperties .= 'Gradient,';
        }
        if(strtolower($rxData['ts_mirror']) == 'yes'){
            $lensProperties .= 'Mirrored,';
        }
        if(strtolower($rxData['ts_polarized']) == 'yes'){
            $lensProperties .= 'Polarized,';
        }
        if(!empty($lensProperties)){
            return substr( $lensProperties, 0, -1);
        }
        return $lensProperties;
    }
    function addLensProperties($rxData)
    {
        $this->magmiData['ts_gradient'] = $rxData['ts_gradient'];
        $this->magmiData['ts_mirror'] = $rxData['ts_mirror'];
        $this->magmiData['ts_polarized'] = $rxData['ts_polarized'];
      //  $this->magmiData['ts_lens_properties'] = $this->addLensProperties($rxData);
    }

}