<?php
abstract class Product
{
    public $magmiData;


    function __construct(ProductData $productData){
        $this->populateProduct($productData->data);
    }

    abstract protected function createSku($productData);


    function populateProduct(Array $productData){
        $sku = $this->createSku($productData);
        $gender = ('men' == strtolower($productData['gender'])) ? 'Man' : $productData['gender'];
        $lensmaterial = ('plast' == strtolower($productData['lens material'])) ? 'Plastic' : $productData['lens material'];

        $this->magmiData = array(
            'sku' => $sku,
            'url_key' => strtolower($sku),
            'type' => 'simple',
            'ts_upc' => $productData['upc'],
            'name' => strtoupper($productData['model name description']),
            'description' => $productData['description'],
            //'short_description' => $productData['short description'],
            'status' => $productData['status'],
            'ts_frame_color' => strtoupper($productData['frame color']),
            'ts_frame_size' => $productData['frame size'],
            'price' => $productData['price'],
            'cost' => $productData['cost'],
            'ts_color_code' => $productData['color code'],
            'ts_lens_color' => $productData['lens color'],
            'ts_lens_base' => $productData['lens base'],
            'ts_lens_material' => $lensmaterial,
            'ts_temple_length' => $productData['temple length'],
            'ts_bridge_size' => $productData['bridge size'],
            'ts_lens_height' => $productData['lens height'],
            'ts_folding' => $productData['folding'],
            'ts_rx_service' => 'NO',
            'ts_shape' => $productData['shape'],
            'ts_frame_type' => $productData['frame type'],
            'ts_temple_material' => $productData['temple material'],
            'ts_front_material' => $productData['front material'],
            'ts_bestseller' => $productData['bestseller'],
            'ts_gender' => $gender,
            'ts_theme' => $productData['theme'],
            'ts_brand' => $productData['brand'],
            'ts_model_name_description' => strtoupper($productData['model name description']),
            'ts_lens_width' => $productData['lens width'],
            'ts_model_number' => $productData['model number'],
            'visibility' => 1,
            'qty' => $productData['quantity'],
            'is_in_stock' => 1,
            'manage_stock' => 1,
            'tax_class_id' => 'none'
        );
    }
    
    function getFramePrice($framePrice){
        $this->magmiData['ts_base_rx_price'] = $framePrice;
    }
}