<?php
/*
 * Extends product
 *
 * Inherited Properties
 * public $magmiData;
 * function populateProduct(Array $productData)
 * abstract protected function createSku($productData)
 *  function addLensProperties($productData);
 */

require_once( 'Product.php' );
class RayBanProduct extends Product
{

    function __construct(RayBanData $productData){
        parent::__construct($productData);
        $this->addLensProperties($productData);
        $this->magmiData['tax_class_id'] = 'Stock Products';
        $this->magmiData['name'] = $productData->data['model code'];
    }
    function createSku($productData){
        $sku = $productData['model code'] . "-" .
            $productData['color code'] . "-" .
            $productData['frame size'];
        return strtoupper(Helper::replaceChars($sku));
    }

    function addLensProperties($productData){
        $this->magmiData['ts_gradient'] = $productData->data['gradient'];
        $this->magmiData['ts_mirror'] = $productData->data['mirror'];
        $this->magmiData['ts_polarized'] = $productData->data['polarized'];
        $this->magmiData['ts_lens_properties'] = $this->getLensProperties($productData);
    }

    //Put together list of lens properties
    function getLensProperties($productData){
        $lensProperties = '';
        if(strtolower( $productData->data['gradient'])  == 'yes'){
            $lensProperties .= 'Gradient, ';
        }
        if(strtolower($productData->data['mirror']) == 'yes'){
            $lensProperties .= 'Mirrored, ';
        }
        if(strtolower($productData->data['polarized']) == 'yes'){
            $lensProperties .= 'Polarized, ';
        }
        if(!empty($lensProperties)){
            return substr( $lensProperties, 0, -2);
        }
        return $lensProperties;
    }
}