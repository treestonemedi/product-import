<?php
 abstract class Image{
    public $sku;
    public $image;
    public $smallImage;
    public $thumbnail;
    public $imageGallery = array();


     function __construct($data){
         $this->sku = $data['sku'];
     }
     abstract function createImages($data);

     function getFileExtension($img){
         $result = glob("../../html/media/import/ray-ban-new-upload-3-8-18/" . $img . ".*" );
         if(sizeof($result) == 1){
             $arr = explode("/", $result[0]);
             $img = $arr[sizeof($arr) - 1];
         }
         return $img;
     }

     function copyImage($directory, $desDirectory){
         $path = "../../html/media/import/$directory/";
         $desPath = "../../html/media/import/$desDirectory/";
         $splitSku = explode("-",$this->sku);
         $file = 'replacement-lenses-' . $splitSku[sizeof($splitSku) - 2] . '-rx.jpeg';
         copy( $path . $file,$desPath . $this->image);
     }
}