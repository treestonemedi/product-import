<?php

class ConfigurableImage extends Image {
    public function createImages($data){
        $image = strtolower(Helper::replaceChars($data['name']) . ".jpg");
        $this->image = $image;
        $this->smallImage = $image;
        $this->thumbnail = $image;
    }
}