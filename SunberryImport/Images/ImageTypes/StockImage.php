<?php

class StockImage extends Image {
    public function createImages($data){
        for($i = 2; $i < 5; $i++){
            $this->imageGallery[] = strtolower(Helper::replaceChars($data['name'] . "-" . $data['ts_color_code']) . "-$i.jpg");
        }
        $this->image = strtolower(Helper::replaceChars($data['name'] . "-" . $data['ts_color_code']) . "-1.jpg");
    }
}