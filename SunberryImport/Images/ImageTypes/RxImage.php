<?php

class RxImage extends Image {

    public function createImages($data){
        $optionNumber = array_slice(explode('-',$this->sku), -2,1)[0];
        $this->image =
            strtolower(
                Helper::replaceChars($data['name'] . "-" .
                    $data['ts_frame_color']) . "-$optionNumber-rx.jpg"
            );
    }
}