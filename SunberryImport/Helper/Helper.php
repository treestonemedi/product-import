<?php

 class Helper{
    public static $images = array();
    public static $classFunctionName = '';

    public static function createProducts($file){
        $products = array();
        //Get Data from file
        $data = DataHelper::getData($file);

        //Check brand
        $brand = '';
        if(isset(current(current($data))[0]['brand'])){
            //Set variables for specific brand classes and functions
            self::$classFunctionName =
                str_replace(' ', '',
                    ucwords(str_replace('-', ' ',
                        strtolower(current(current($data))[0]['brand']))));
            $headerClass = self::$classFunctionName. 'Headers';

            //Check headers against one product.
            new $headerClass($data);
            $products = self::createBrandProducts($data);
        }else{
            exit('You must supply a brand');
        }

        ImageHelper::createImageCsv(self::$images);
        return $products;
    }


     public static function createBrandProducts($data)
     {
         $brandData = self::$classFunctionName . "Data";

         $brandConfigurableProduct = self::$classFunctionName . "ConfigurableProduct";
         $framePrice = '';
         $products = array();
         $simpleProducts = array();
         foreach ($data as $model => $productGroup) {
             $simpleSkus = array();
             foreach ($productGroup as $colorSize => $items) {
                 $simpleProducts = self::createBrandSimpleProducts($productGroup[$colorSize]);
                 foreach ($simpleProducts as $product) {
                     $simpleSkus[] = $product->magmiData['sku'];
                 }
                 $products = array_merge(
                     $products,
                     $simpleProducts
                 );
                 foreach ($productGroup[$colorSize] as $prod){
                     if($prod['frame price'] != ''){
                        $framePrice =  $prod['frame price'];
                     }
                 }
             }

             $productData = new $brandData(current($productGroup)[0]);

             if ($configProduct = new $brandConfigurableProduct($productData, self::getLowestPrice($products))) {
                 if($productData->data['brand'] === 'Oakley' && $productData->data['configurable sku'] === ''){
                     $productData->data['frame color'] = "LENSES ONLY NO FRAME";
                     $productData->data['frame price'] = 0;
                     $productData->data['rx frame cost'] = 0;
                     if($rxProducts = RxHelper::createRxProducts($productData, $products[0], (new OakleyRxData())->rxOptions))
                         foreach($rxProducts as $product){
                             $image = new RxImage($product->magmiData);
                             $image->createImages($product->magmiData);
                             self::$images[] = $image;
                             $image->copyImage(
                                 'Replacement-Lenses-Images',
                                 $GLOBALS['globalValues']['imageDirectory']
                             );
                             $simpleSkus[] = $product->magmiData['sku'];
                         }

                     self::copySwatchImage(
                         'Replacement-Lenses-Images',
                         $GLOBALS['globalValues']['imageDirectory'],
                         $productData->data['model name description']
                     );
                     $products = array_merge(
                         $products,
                         $rxProducts
                     );
                 }
                 $configProduct->checkSimpleSkus($simpleProducts);
                 $configProduct->getSimpleSkus($simpleSkus);
                 $configProduct->getFramePrice($framePrice);
                 $image = new ConfigurableImage($configProduct->magmiData);
                 if($productData->data['configurable sku'] == ''){
                     $image->createImages($configProduct->magmiData);
                     self::$images[] = $image;
                 }
                 $products[] = $configProduct;
             }

         }

         return $products;
     }

     /**
      * @param $data
      * @param $products
      * @return array
      */

     public static function createBrandSimpleProducts($data)
     {
        // $createBrandSimpleProducts = "create" . self::$classFunctionName . "SimpleProducts";
         $brandData = self::$classFunctionName . "Data";
         $brandRxData = self::$classFunctionName . "RxData";
         $brandProduct = self::$classFunctionName . "Product";

         $products = array();
         $productData = false;
         $baseProduct = false;
         foreach ($data as $item){
             $productData = new $brandData($item);

             $baseProduct = new $brandProduct($productData);

             //Check If stock item
             if ($productData->isStock) {
                 if ($baseProduct) {
                     $products[] = $baseProduct;
                 }
             }
         }
         foreach($products as $product){
             $image = new StockImage($product->magmiData);
             $image->createImages($product->magmiData);
             self::$images[] = $image;
         }
         //Check if rx item
         if ($productData->hasRx) {
             $rxData = new $brandRxData();
             if($rxProducts = RxHelper::createRxProducts($productData, $baseProduct, $rxData->rxOptions))
                 foreach($rxProducts as $product){
                     $image = new RxImage($product->magmiData);
                     $image->createImages($product->magmiData);
                     self::$images[] = $image;
                 }

                 $products = array_merge(
                     $products,
                     $rxProducts
                 );
         }
         return $products;
     }

     public static function replaceChars($string){
         $string = str_replace(array(" ", ".", "/"), '-', $string);
         $string = str_replace(array("(","'", ")", "&", "#"), '', $string);
         $string = str_replace("--", "-", $string);
         return $string;
     }

     public static function getAllAttributes(Array $products){
         $attributes = array();
         foreach  ($products as $product){
             foreach ($product->magmiData as $key => $magmiDatum) {
                 if(!in_array($key, $attributes)){
                     $attributes[] = $key;
                 }
             }
         }
         return $attributes;
     }
     public static function getOldConfigAttributes(Array $products){
         $attributes = array();
         foreach  ($products as $product){
             if(isset($product->magentoProduct)){
                 foreach ($product->magmiData as $key => $magmiDatum) {
                     if(!in_array($key, $attributes)){
                         $attributes[] = $key;
                     }
                 }
             }

         }
         return $attributes;
     }

     public static function createMagmiCsv(Array $products){

         $attributes = self::getAllAttributes($products);
         $oldConfigAttributes = self::getOldConfigAttributes($products);
         $importName = GlobalHelper::getImportName($GLOBALS['globalValues']);

         $fileName = $importName . '-file.csv';
         $fp = fopen('var/export/' . $fileName, 'w');
         $GLOBALS['globalValues']['magmiFile'] = $fileName;

         $configFileName = $importName . '-configurable-file.csv';
         $cfp = fopen('var/export/' . $configFileName, 'w');
         $GLOBALS['globalValues']['magmiConfigFile'] = $configFileName;

         $swatchesFileName = $importName . '-swatches-file.csv';
         $sfp = fopen('var/export/' . $swatchesFileName, 'w');
         $GLOBALS['globalValues']['swatchesFile'] = $swatchesFileName;

         $linksFileName = $importName . '-links-file.csv';
         $linksFile = fopen('var/export/' . $linksFileName, 'w');
         $GLOBALS['globalValues']['linksFile'] = $linksFileName;


         fputcsv($fp, $attributes);
         fputcsv($cfp, $oldConfigAttributes);
         fputcsv($sfp, array('name : frame color', 'swatch file name'));

         $swatch = '';
         $url = '';
         $frameColors = array();

         foreach ($products as $product){
             if($product->magmiData['type'] != 'configurable'){
                 if(self::createSwatchFile($product) != $swatch){
                     $swatch = self::createSwatchFile($product);
                     fputcsv($sfp, array(
                         $product->magmiData['name']. ' : ' . $product->magmiData['ts_frame_color'] ,
                         $swatch
                     ));
                 }
             }
             $useAttributes = $attributes;
             $useFile = $fp;
            if(isset($product->magentoProduct) && $product->magmiData['type'] == 'configurable'){
                $useAttributes = $oldConfigAttributes;
                $useFile = $cfp;
            }
            $csvLine = array();
            foreach($useAttributes as $attribute ){
                if(key_exists( $attribute,$product->magmiData)){
                    $csvLine[] = $product->magmiData[$attribute];
                }else{
                    $csvLine[] = '';
                }
            }
            fputcsv($useFile, $csvLine);
             if($product->magmiData['type'] != 'configurable'){
                 $frameColors[$product->magmiData['ts_frame_color']] =  $product->magmiData['ts_rx_service'];

             }elseif ($product->magmiData['type'] == 'configurable'){
                 foreach ($frameColors as $frameColor => $rxService){
                     $newUrl = self::createUrl(strtolower($product->magmiData['sku']), $frameColor,$rxService);
                     if($url != $newUrl){
                         fputcsv($linksFile, array($newUrl));
                         $url = $newUrl;
                     }
                 }
                 $frameColors = array();
             }

         }
         fclose($fp);
         fclose($cfp);
         fclose($sfp);
         fclose($linksFile);
     }

     public static function getLowestPrice($products){
         $lowestPrice = 1000;
         foreach ($products as $product) {
             if($product->magmiData['price'] < $lowestPrice){
                 $lowestPrice = $product->magmiData['price'];
             }
         }
         return $lowestPrice;
     }

     public static function createSwatchFile($product){
         return strtolower(
             Helper::replaceChars(
                 $product->magmiData['name'] . "-" .
                 $product->magmiData['ts_frame_color']
             ).
             ($product->magmiData['ts_rx_service'] == 'YES' ? '-rx.png' : '.png')
         );
     }

     public static function createUrl($urlKey, $frameColor,$rxService){
         $link =  'https://test.sunberryrx.com/' .
             $urlKey . '.html?framecolor=' .
             str_replace(' ', '%20',$frameColor ) . '&rxservice=';
         $link .= (strtolower($rxService) == 'yes')? 'yes' : 'no';
         return $link;
     }

    public static function copySwatchImage($directory, $desDirectory, $modelName){
        $path = "../../html/media/import/$directory/";
        $desPath = "../../html/media/import/$desDirectory/Swatches/";
        if(!file_exists ( $desPath )){
            mkdir($desPath,    0750, true);
        }
        $file = 'lenses-only-no-frame-rx.png';
        $modelName = strtolower(str_replace(' ', '-', $modelName));
        $image = "$modelName-$file";
        copy( $path . $file,$desPath . $image);
     }

 }