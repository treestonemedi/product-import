<?php
/**
 * Created by PhpStorm.
 * User: msomm
 * Date: 9/7/2018
 * Time: 1:52 PM
 */

class GlobalHelper
{
    public static function writeToFile($array){
        $handle = fopen('global.txt', 'w');
        foreach ($array as $key =>$val){
            fputcsv($handle, array($key,$val));
        }
    }

    public static function getImportName($globalValues){
        return explode('/',$globalValues['imageDirectory'])[1];
    }

    /**
     * Retrieves global variables from global.txt
     * @return array ('brand' => $brand, 'imageDirectory' => $directory, 'file', $file);
     **/
    public static function readGlobalValues(){
        $globalValues = array();
        $handle = fopen('global.txt', 'r');
        while (($line = fgetcsv($handle)) !== FALSE) {
            $globalValues[$line[0]] = $line[1];
        }
        return $globalValues;
    }

}