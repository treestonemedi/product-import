<?php
/**
 * Created by PhpStorm.
 * User: msomm
 * Date: 1/11/2018
 * Time: 10:05 AM
 */
class DataHelper{
    static function getData($fileName){
        $headers = array();
        $first = true;
        $handle = fopen($fileName, 'r');
        $modelGroups = array();
        while (($line = fgetcsv($handle)) !== FALSE) {
            if(!$first){
                $productData = [];
                for($i = 0; $i < sizeof($headers); $i++){
                    if(key_exists($i,$line)){
                        $productData[$headers[$i]] = trim($line[$i]);
                    }else{
                        $productData[$headers[$i]] = '';
                    }
                }if(isset($productData)){
                    $colorSize = $productData['frame color'] . "-" .$productData['frame size'];
                    switch (strtolower($productData['brand'])){
                        case 'ray-ban':
                            $model = $productData['model code'];
                            break;
                        case 'oakley':
                            $model = $productData['model name description'];
                            break;
                    }
                    $modelGroups[$model][$colorSize][] = $productData;
                }
            }else{
                $badCharacters = chr(  187 ) . chr(191) . chr(239);
                foreach ($line as $cell){
                    $cell = preg_replace('!\s+!', ' ', $cell);
                    $headers[] = trim(trim($cell),$badCharacters);
                }
                $first = !$first;
            }
        }
        fclose($handle);
        return $modelGroups;
    }

    public static function checkHeaders($data, $headers){
        $missingHeaders = '';
        $extraHeaders = '';
        foreach ($headers as $head){
            if(!key_exists($head, $data)){
                $missingHeaders .= $head . " ";
            }
        }
        foreach ($data as $key => $value){
            if(!in_array($key, $headers)){
                $extraHeaders .= $key . " ";
            }
        }
        if(!empty($missingHeaders)){
            exit ("You're missing the following headers: " . $missingHeaders);
        }
        if(!empty($extraHeaders)){
            exit (" You have extra headers " . $extraHeaders);
        }
    }
}

