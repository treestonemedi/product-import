<?php
/**
 * Created by PhpStorm.
 * User: msomm
 * Date: 1/26/2018
 * Time: 1:46 PM
 */

class ImageHelper
{

    public static function createImageCsv(Array $images){
        $headers = array('sku','image','small_image','thumbnail','media_gallery' );
        $directory = $GLOBALS['globalValues']['imageDirectory'];
        $importName = GlobalHelper::getImportName($GLOBALS['globalValues']);

        $imagesFileName = $importName . 'images-file.csv';
        $fp = fopen('var/export/' . $imagesFileName, 'w');
        $GLOBALS['globalValues']['imagesFile'] = $imagesFileName;

        $imagesSunberryFileName = $importName . 'images-sunberry-file.csv';
        $fps = fopen('var/export/' . $imagesSunberryFileName, 'w');
        $GLOBALS['globalValues']['imagesSunberryFile'] = $imagesSunberryFileName;


        fputcsv($fp, $headers);
        fputcsv($fps, $headers);
        foreach ($images as $image){
            $csvLine = array(
                $image->sku,
                isset($image->image) ? "+$directory/$image->image"  : '',
                isset($image->smallImage) ? "+$directory/$image->smallImage" : '',
                isset($image->thumbnail) ? "+$directory/$image->thumbnail" : ''
            );
            $imageGallery = '';
            foreach($image->imageGallery as $galleryImage ){
                $imageGallery .= $directory . '/' . $galleryImage.";";
            }
            $csvLine[] = $imageGallery;
            fputcsv($fp, $csvLine);
            if(!self::checkForCopiedImage($image->sku) && !self::checkForLensesOnly($image->sku)){
                fputcsv($fps, $csvLine);
            }
        }
    }


    public static function checkForCopiedImage($sku){

        $copiedImages = array('19-RX','20-RX','21-RX','23-RX','24-RX','25-RX','27-RX','28-RX','30-RX','31-RX','33-RX',
            '34-RX','35-RX','43-RX','46-RX','47-RX','48-RX','49-RX', '50-RX','51-RX','52-RX','53-RX','54-RX');

        foreach ($copiedImages as $copiedEnd){
          if (strpos($sku, $copiedEnd)  > -1){
              return true;
          }
        }
        return false;
    }

    public static function checkForLensesOnly($sku){
        if (strpos(strtolower($sku), 'lenses-only-no-frame-color') > -1){
            return true;
        }
        return false;
    }
}
