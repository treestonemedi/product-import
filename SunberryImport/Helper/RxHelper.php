<?php
/**
 * Created by PhpStorm.
 * User: msomm
 * Date: 1/9/2018
 * Time: 9:41 AM
 */
class RxHelper{
    public static function createRxProducts($productData, $baseProduct, $rxOptions){
        $classFunctionName =
            str_replace(' ', '',
                ucwords(str_replace('-', ' ',
                    strtolower($productData->data['brand']))));
        $rxProductClass = $classFunctionName. 'RxProduct';
        //Actual option numbers is number plus 1 - 4,9,18,26,29,36
        $discontinuedOptions = array(3,8,17,25,28,35);
        $shieldOptions = array(0,2,4,18,20);
        $shieldPrices = array(
            0 => array('310','0','135'),
            2 => array('310','0','135'),
            4 => array('356','0','155'),
            18 => array('356','0','155'),
            20 => array('402','0','175')
        );

        $isShield = self::checkShield($productData->data['model name description']);

        $rxProducts = array();
        foreach ($rxOptions as $key =>  $rxOption) {
            if((in_array($key, $discontinuedOptions) && $productData->data['brand'] == 'Oakley') || ($isShield && !in_array($key, $shieldOptions))){
               continue;
            }
            if($isShield && in_array($key, $shieldOptions)){
                $rxOption['price'] = $shieldPrices[$key][0];
                $rxOption['ts_progressive_price'] = $shieldPrices[$key][1];
                $rxOption['ts_rx_lens_cost'] = $shieldPrices[$key][2];
            }
            if($rxProduct = new $rxProductClass($productData, $baseProduct, $rxOption, $key + 1)){
                $rxProducts[] =  $rxProduct;
            }
        }
        return $rxProducts;
    }

    public static function checkShield($modelNameDescription){
        if(
            $modelNameDescription &&
            in_array(strtolower($modelNameDescription), array('radar ev path', 'm2 frame', 'jawbreaker'))){
            return true;
        }
        return false;
    }
}