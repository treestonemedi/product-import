<?php
/**
 * Created by PhpStorm.
 * User: msomm
 * Date: 9/12/2018
 * Time: 12:19 PM
 */

class CheckImagesHelper
{

    /**
     * @var array Images that are copied
     */
    public $imagesToCopy = array(
            '-2-' => array('-20-'),
            '-3-' => array('-19-'),
            '-5-' => array('-21-','-43-','-50-'),
            '-6-' => array('-23-'),
            '-7-' => array('-24-'),
            '-8-' => array('-25-'),
            '-10-' => array('-27-'),
            '-11-' => array('-28-','-46-', '-51-'),
            '-13-' => array('-30-','-49-','-52-'),
            '-14-' => array('-31-','-47-','-53-'),
            '-15-' => array('-34-', '-48-','-54-'),
            '-16-' => array('-33-'),
            '-17-' => array('-35-')
        );
    /**
     * Checks if image file exists regardless of extension and writes missing image to file
     * @param resource $newMissingFileHandle Handle of file to write missing images to
     * @param array $files List of files in image directory
     * @param string $sku The sku this image is for
     * @param string $image Path to Image in media import
     * @return bool returns correctImageName or false if not found
     */
    public function checkImageFile($newMissingFileHandle,$files,$sku, $image){
        $splitPath = explode('/',$image);
        $splitImg = explode('.',$splitPath[count($splitPath) - 1]);
        $csvFiles =  preg_grep('/.*' . $splitImg[0] . '\..*$/i', $files);
        if(!isset($csvFiles) || sizeof($csvFiles) == 0){
            fputcsv($newMissingFileHandle, array($sku,$splitImg[0]));
            return false;
        }elseif(sizeof($csvFiles) > 0){
            $arr = explode("/", current($csvFiles));
            return $arr[sizeof($arr) - 1];

        }
        return false;
    }

    /**
     * Checks if gallery images file exists regardless of extension  and writes missing image to file
     * @param resource $newMissingFileHandle Handle of file to write missing images to
     * @param array $line with sku as index 0 followed by gallery as index 4
     * @param array $files List of files in image directory
     * @return array returns  array of correctImageNames or empty array if not found
     */
    public function checkImageGalleryFiles($newMissingFileHandle,$line,$files){
        $galleryImages = explode(';',$line[4]);
        $correctImages = array();
        foreach ($galleryImages as $img) {
            $correctFile = $this->checkImageFile($newMissingFileHandle,$files,$line[0],$img);
            if($correctFile !== false){
                $correctImages[] = $correctFile;
            }
        }
        return $correctImages;
    }

    /**
     * Checks if swatch image file exists with extension and writes missing image to file
     * @param resource $newMissingFileHandle Handle of file to write missing images to
     * @param array $line with Model - Frame color as index 0 followed by image as index 1
     * @param array $files List of files in image directory
     */
    public function checkSwatchesFiles($newMissingFileHandle,$line,$files){
        $csvFiles =  preg_grep('/.*' . $line[1] . '/i', $files);
        if(!isset($csvFiles) || sizeof($csvFiles) == 0){
            fputcsv($newMissingFileHandle, array($line[0],$line[1]));
        }
    }


    /**
     * Copies all images in $this->imagesToCopy
     * @param string $path path to image directory
     */
    function copyImages ($path)
    {
        if ($handle = opendir($path)) {
            while (false !== ($fileName = readdir($handle))) {
                foreach ($this->imagesToCopy as $from => $to)
                    if (strpos($fileName, $from)) {
                        foreach ($to as $item) {
                            $newName = str_replace($from, $item, $fileName);
                            copy($path . $fileName, $path . $newName);
                        }
                    }
            }
            closedir($handle);
        }
    }


}