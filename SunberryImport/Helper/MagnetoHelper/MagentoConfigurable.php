<?php
class MagentoConfigurable{

    function load($configurableSku){
        if(
        $prod = Mage::getModel('catalog/product')
            ->loadByAttribute('sku',$configurableSku)){
            return $prod;
        }
        exit($configurableSku . ' is not an existing sku In magneto');
    }
    function notExist($configurableSku){
        if(
        $prod = Mage::getModel('catalog/product')
            ->loadByAttribute('sku',$configurableSku)) {
            exit($configurableSku . ' already exists In magneto');
        }
        return;
    }
}