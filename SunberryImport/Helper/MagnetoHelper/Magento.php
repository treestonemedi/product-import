<?php
class Magento{
    function __construct()
    {
        require_once '../../html/app/Mage.php';
        umask(0);
        /* not Mage::run(); */
        Mage::app('default');
        Mage::getConfig()->init();
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

    }
}