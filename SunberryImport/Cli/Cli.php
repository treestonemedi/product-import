<?php
/**
 * Created by PhpStorm.
 * User: msomm
 * Date: 9/7/2018
 * Time: 11:40 AM
 */

class Cli
{
    /**
     * Retrieves global variables through calling $this->getResponse on image directory and file
     * @return array ('brand' => $brand, 'imageDirectory' => $directory, 'file', $file);
     **/
    public function getGlobalValues ()
    {
        //Get Brand From User
        $validBrands = array('Oakley', 'Ray-Ban');
        $brand = $this->getResponse($validBrands, 'Please choose a brand:');

        //Get Image Directories for given brand and remove beginning of path
        $directories = glob("../../html/media/import/$brand-New-Product/*", GLOB_ONLYDIR);
        $directories = $this->arrayRemoveValues($directories, '../../html/media/import/');

        $this->checkArrayHasValues($directories, "No directories were found in /media/import/$brand-New-Product/");
        //Get Image Directory  From User
        $directory = $this->getResponse($directories, 'Which directory are your images in:');

        //Get Sunberry Supplied Data
        $files = glob("var/import/*");
        $files = $this->arrayRemoveValues($files, 'var/import/');

        $this->checkArrayHasValues($files, 'No files found in SunberryImport/var/import/');
        //Get File to run off of from User
        $file = $this->getResponse($files, 'Which file are you running the script off:');

        return array('brand' => $brand, 'imageDirectory' => $directory, 'file'=> $file);
    }

    /**
     * Prompts user whether to run next step
     * @param integer $number next step
     * @return string Yes or No to continue to next step
     */
    public function checkNextStep($number){
        switch($number){
            case 2:
                $promptHeader = 'Do you want to run next step: checkSunberryImages.php';
                break;
            case 3:
                $promptHeader = 'Do you want to run next step: copyAndCheckImages.php';
                break;
        }
        if(isset($promptHeader)){
            return $this->getResponse(array('Yes'), $promptHeader);
        }
        exit('Stopping after step ' . $number - 1);
    }

    /**
     * Runs next step
     * @param integer $number step to run
     * @return bool $isMissing true if any images are missing
     */
    public function runNextStep($number){
        switch($number){
            case 2:
                $isMissing = Scripts::runStep2();
                break;
            case 3:
                $isMissing = Scripts::runStep3();
                break;
            default:
                exit('Stopping after step ' . $number - 1);
        }
        return $isMissing;
    }

    /**
     * Gets user input from list of $validResponses
     * @param $validResponses  array of valid responses as $response => $value
     * @param $promptHeader string Header of prompt
     * @return string 'value of response'
     **/
    private function getResponse ($validResponses,$promptHeader = '')
    {
        $handle = fopen("php://stdin", "r");
        while(!isset($response) || $response === false) {
            $prompt = "$promptHeader\n" ;
            foreach ($validResponses as $validResponse =>  $value){
                $prompt .= "$validResponse:$value \n";
            }
            echo $prompt . "e:exit\n";
            $line = strtolower(trim(fgets($handle)));
            if(key_exists($line,$validResponses)){
                fclose($handle);
                return $validResponses[$line];
            }elseif($line === 'e'){
                exit;
            }
            echo "Please enter a valid number.\n";
            $response = false;
        }
        fclose($handle);
        return false;
    }

    /**
     * Checks  $array has and outputs error message on failure
     * @param $array  array of valid responses as $index => $value
     * @param $errorMessage string message to return on error
     **/
    private function checkArrayHasValues($array,$errorMessage){
        if(!isset($array) || count($array) === 0){
            exit($errorMessage);
        }
    }

    /**
     * Removes string from all elements in array
     * @param array $array
     * @param string $strToRemove
     * @return array
     */
    private function arrayRemoveValues ($array, $strToRemove)
    {
        foreach($array as $index => $element){
            $array[$index] = str_replace($strToRemove, '', $element);
        }
        return $array;
    }

}