<?php
/**
 * Created by PhpStorm.
 * User: msomm
 * Date: 9/13/2018
 * Time: 2:10 PM
 */

class Scripts
{
    /**
     * Checks that all images and swatches provided by client are in image directory.
     * The following two files are checked
     * 1.var/export/{import name}images-sunberry-file.csv(name retrieved from global.txt)
     * 2.var/export/{import name}swatches-file.csv(name retrieved from global.txt)
     * Outputs list of missing images and swatches to SunberryImport/missing{import name}.txt
     *
     * @return bool true if any images are missing
     */
   public static function runStep2(){
       //Instantiate CheckImagesHelper for use of checkImageFile  and checkImageGalleryFiles functions
       $checkHelper = new CheckImagesHelper();

        //Retrieve global values from global.txt
       $globalValues = GlobalHelper::readGlobalValues();

        //Image directory
       $directory = $globalValues['imageDirectory'];

       $fileName = $globalValues['imagesSunberryFile'];
       $swatchesFileName = $globalValues['swatchesFile'];
       $missingFile = 'missing-' . GlobalHelper::getImportName($globalValues) . '.txt';

       $handle = fopen('var/export/' . $fileName, 'r');
       $swatchesHandle = fopen('var/export/' . $swatchesFileName, 'r');
       $newMissingFileHandle = fopen($missingFile , "w");

        //Retrieve list of image files in /html/media/import/{Image Directory}/
       $files = glob("../../html/media/import/$directory/*");
        //Retrieve list of swatch files in /html/media/import/{Image Directory}/Swatches/
       $swatchFiles = glob("../../html/media/import/$directory/Swatches/*");

       $isMissingImages = false;
        //Check images
       while (($line = fgetcsv($handle)) !== FALSE) {
           //Write headers to missingFile
           if($line[0] == 'sku'){
               fputcsv($newMissingFileHandle, array('sku','missing image'));
               continue;
           }
           if($line[1] != ''){
               //Checks for image, writes missing image to file and returns false if not found
               $isMissingImages = !$checkHelper->checkImageFile($newMissingFileHandle,$files,$line[0], $line[1]);
           }
           if($line[4] != ''){
               //Calls $checkHelper->checkImageFile on each image in gallery,
               // writes missing images to file and returns array of found images
               $foundGallery = $checkHelper->checkImageGalleryFiles($newMissingFileHandle,$line,$files);
               $isMissingImages = (count($foundGallery) === count(explode(';',$line[4])))? false : true;
           }
       }

        //Check swatches
       while (($line = fgetcsv($swatchesHandle)) !== FALSE) {
           //Write swatch headers to missingFile
           if($line[0] == 'name : frame color'){
                   fputcsv($newMissingFileHandle, array($line[0],$line[1]));
               continue;
           }
           if($line[1] != '') {
               //Checks for swatch image, writes missing image to file
               $checkHelper->checkSwatchesFiles($newMissingFileHandle, $line, $swatchFiles);
           }
       }

       fclose($handle);
       fclose($swatchesHandle);
       fclose($newMissingFileHandle);

       return $isMissingImages;
   }

    /**
     * This step does three functions.
     * 1.Copies all rx images found in CheckImagesHelper->imagesToCopy
     * 2.Checks all images in var/export/{import name}images-file.csv (name retrieved from global.txt) are found in
     *  the image directory - if any are missing outputs list of missing images - missing-{import name}images-file.csv
     * 3.Outputs file with correct extensions to /var/export/corrected-{Import name*}-images-file.csv
     *
     * @return bool true if any images are missing
     */
    public static function runStep3(){

        //Instantiate CheckImagesHelper for use of checkImageFile  and checkImageGalleryFiles functions
        $checkHelper = new CheckImagesHelper();

        //Retrieve global values from global.txt
        $globalValues = GlobalHelper::readGlobalValues();

        //Image directory
        $directory = $globalValues['imageDirectory'];

        //Relative path to Image directory
        $path = "../../html/media/import/$directory/";

        $fileName = $globalValues['imagesFile'];
        $missingFile = 'missing-' . GlobalHelper::getImportName($globalValues) . '.txt';
        $handle = fopen('var/export/' . $fileName, 'r');
        $newMissingFileHandle = fopen($missingFile , "w");
        $correctImagesHandle = fopen('var/export/corrected-' . $fileName, 'w');

        if($globalValues['brand'] == 'Oakley'){
            $checkHelper->copyImages($path);
        }

        //Retrieve list of files in /html/media/import/{Image Directory}/
        $files = glob("../../html/media/import/$directory/*");

        $isMissing = false;
        while (($line = fgetcsv($handle)) !== FALSE) {
            if($line[0] == 'sku'){
                fputcsv($correctImagesHandle, $line);
                continue;
            }
            if($line[1] != '' && $line[0] != 'sku'){
                //Checks for image and returns correct fileName with extension if not found returns false
                $correctImage = $checkHelper->checkImageFile($newMissingFileHandle,$files,$line[0], $line[1]);
                if($correctImage != false){
                    $line[1] = "+$directory/" . $correctImage;
                    if($line[2] != ''){
                        $line[2] = $line[1];
                        $line[3] = $line[1];
                    }
                    fputcsv($correctImagesHandle, $line);
                }else{
                    $isMissing = true;
                }
            }
            if($line[4] != '' && $line[0] != 'sku'){
                //Calls $checkHelper->checkImageFile on each image in gallery and returns array of found images
                $correctImages = $checkHelper->checkImageGalleryFiles($newMissingFileHandle,$line,$files);

                //If new images is same size as original (i.e. all were found)
                if(count($correctImages) === count(explode(';',$line[4]))){
                    $line[4] = "$directory/" . implode($correctImages,";$directory/" );
                    fputcsv($correctImagesHandle, $line);
                }else{
                    $isMissing = true;
                }
            }

        }

        fclose($handle);
        fclose($newMissingFileHandle);
        fclose($correctImagesHandle);

        return $isMissing;
    }
}