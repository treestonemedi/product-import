<?php
/**
 * Extends RxData Extends ProductData
 *
 * Inherits the following properties from ProductData
 * $data;
 * $hasRx;
 * $isStock;
 * $firstTime;
 */
class RayBanRxData Extends RxData {

    // $rxOptions (color, polarized,  gradient , mirror, price, cost);
    public $rxOptions = array(
        array('GREEN','YES','NO','NO','270','100'),
        array('BROWN','YES','NO','NO','270','100'),
        array('BLUE','YES','YES','NO','320','118.52'),
        array('GREY','YES','YES','NO','320','118.52'),
        array('BROWN','YES','YES','NO','320','118.52'),
        array('BLUE','YES','NO','YES','320','118.52'),
        array('SILVER','YES','NO','YES','320','118.52'),
        array('ORANGE','YES','NO','YES','320','118.52'),
        array('GREEN','NO','NO','NO','200','74.06'),
        array('BROWN','NO','NO','NO','200','74.06'),
        array('BLUE','NO','NO','NO','200','74.06'),
        array('BLUE','NO','NO','YES','250','92.59'),
        array('SILVER','NO','NO','YES','250',92.59),
        array('COPPER','NO','NO','YES','250','92.59')
    );


    function __construct()
    {
        parent::__construct();
        $this->getRxProperties();
    }

    function getRxProperties(){
        for ($i = 0; $i < sizeof($this->rxOptions); $i++) {
            $this->rxOptions[$i]['ts_lens_color'] = $this->rxOptions[$i][0];
            $this->rxOptions[$i]['ts_polarized'] = $this->rxOptions[$i][1];
            $this->rxOptions[$i]['ts_gradient'] = $this->rxOptions[$i][2];
            $this->rxOptions[$i]['ts_mirror'] = $this->rxOptions[$i][3];
            $this->rxOptions[$i]['price'] = $this->rxOptions[$i][4];
            $this->rxOptions[$i]['ts_rx_lens_cost'] = $this->rxOptions[$i][5];
        }
    }
}