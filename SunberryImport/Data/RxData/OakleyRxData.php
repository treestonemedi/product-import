<?php
/**
 * Extends RxData Extends ProductData
 *
 * Inherits the following properties from ProductData
 * $data;
 * $hasRx;
 * $isStock;
 * $firstTime;
 */

require_once( 'RxData.php' );
class OakleyRxData extends RxData
{
    public $rxOptions = array(
        array('CLEAR', '184', '334','80'),
        array('BRONZE', '230', '334','100'),
        array('GREY', '230', '334','100'),
        array('PERSIMMON', '230', '334','100'),
        array('BLACK IRIDIUM', '276', '380','120'),
        array('EMERALD IRIDIUM', '276', '380','120'),
        array('FIRE IRIDIUM', '276', '380','120'),
        array('24K IRIDIUM', '276', '380','120'),
        array('GOLD IRIDIUM', '276', '380','120'),
        array('ICE IRIDIUM', '276', '380','120'),
        array('JADE IRIDIUM', '276', '380','120'),
        array('RED IRIDIUM', '276', '380','120'),
        array('RUBY IRIDIUM', '276', '380','120'),
        array('SAPPHIRE IRIDIUM', '276', '380','120'),
        array('TUNGSTEN IRIDIUM', '276', '380','120'),
        array('TORCH IRIDIUM', '276', '380','120'),
        array('VIOLET IRIDIUM', '276', '380','120'),
        array('VR28 BLACK IRIDIUM', '276', '380','120'),
        array('GREY POLARIZED', '276', '449','120'),
        array('BRONZE POLARIZED', '276', '449','120'),
        array('BLACK IRIDIUM POLARIZED', '322', '495','140'),
        array('DEEP BLUE IRIDIUM POLAR', '322', '495','140'),
        array('EMERALD IRIDIUM POLAR', '322', '495','140'),
        array('FIRE IRIDIUM POLAR', '322', '495','140'),
        array('24K IRIDIUM POLAR', '322', '495','140'),
        array('GOLD IRIDIUM POLAR', '322', '495','140'),
        array('ICE IRIDIUM POLAR', '322', '495','140'),
        array('JADE IRIDIUM POLAR', '322', '495','140'),
        array('OO BLACK IRIDIUM POLAR', '322', '495','140'),
        array('RUBY IRIDIUM POLAR', '322', '495','140'),
        array('SAPPHIRE IRIDIUM POLAR', '322', '495','140'),
        array('SHALLOW BLUE IRIDIUM POLAR', '322', '495','140'),
        array('TORCH IRIDIUM POLAR', '322', '495','140'),
        array('TUNGSTEN IRIDIUM POLAR', '322', '495','140'),
        array('VIOLET IRIDIUM POLAR', '322', '495','140'),
        array('VR28 BLACK IRIDIUM POLAR', '322', '495','140'),
        array('PRIZM ROAD', '322', '495','140'),
        array('PRIZM TRAIL', '322', '495','140'),
        array('PRIZM GOLF', '322', '495','140'),
        array('PRIZM FIELD', '322', '495','140'),
        array('CLEAR TO BROWN', '310', '483','135'),
        array('CLEAR TO GREY', '310', '483','135'),
        array('PRIZM BLACK POLARIZED', '368', '540','160'),
        array('PRIZM DARK GOLF', '322', '494','140'),
        array('PRIZM DEEP WATER POLARIZED', '368', '540','160'),
        array('PRIZM JADE POLARIZED', '368', '540','160'),
        array('PRIZM SAPPHIRE POLARIZED', '368', '540','160'),
        array('PRIZM TUNGSTEN POLARIZED', '368', '540','160'),
        array('PRIZM RUBY POLARIZED', '368', '540','160'),
        array('PRIZM BLACK', '322', '495','140'),
        array('PRIZM JADE', '322', '495','140'),
        array('PRIZM RUBY', '322', '495','140'),
        array('PRIZM SAPPHIRE', '322', '495','140'),
        array('PRIZM TUNGSTEN', '322', '495','140')
    );

    function __construct()
    {
        parent::__construct();
        $this->getRxProperties();
    }

    function getRxProperties(){
        for ($i = 0; $i < sizeof( $this->rxOptions); $i++) {
            $this->rxOptions[$i]['ts_prizm'] = "NO";
            $this->rxOptions[$i]['ts_transition'] = "NO";
            $this->rxOptions[$i]['ts_iridium'] = "NO";
            $this->rxOptions[$i]['ts_polarized'] = "NO";
            if ((strpos($this->rxOptions[$i][0], 'PRIZM') !== false)) {
                $this->rxOptions[$i]['ts_prizm'] = "YES";
            }
            if ((strpos($this->rxOptions[$i][0], ' TO ') !== false)) {
                $this->rxOptions[$i]['ts_transition'] = "YES";
            }
            if ((strpos($this->rxOptions[$i][0], 'IRIDIUM') !== false)) {
                $this->rxOptions[$i]['ts_iridium'] = "YES";
            }
            if ((strpos($this->rxOptions[$i][0], 'POLAR') !== false)) {
                $this->rxOptions[$i]['ts_polarized'] = "YES";
            }
            $this->rxOptions[$i]['price'] = $this->rxOptions[$i][1];
            $this->rxOptions[$i]['ts_progressive_price'] = $this->rxOptions[$i][2];
            $this->rxOptions[$i]['ts_rx_lens_cost'] = $this->rxOptions[$i][3];
            $this->rxOptions[$i]['ts_lens_color'] = $this->getLensColor($this->rxOptions[$i][0]);
        }
    }

    function getLensColor($lensColor){
        foreach (array("POLARIZED", "POLAR", "PRIZM") as $word) {
            $lensColor = str_replace($word, "", $lensColor);
        }
        return trim($lensColor);
    }

}