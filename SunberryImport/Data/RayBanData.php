<?php
/**
 * Extends ProductData
 * Inherits the following properties from ProductData
 * $data;
 * $hasRx;
 * $isStock;
 * $firstTime;
 */
require_once( 'ProductData.php' );
class RayBanData extends ProductData
{
    function __construct($data){
        parent::__construct ($data);
        $this->checkData();
    }

    function checkData()
    {
        $firstUppercase = array('shape', 'gender', 'frame type',' front material', 'lens material');
        $requiredList =array(
            'model code','frame color','gender','brand','status','rx service','stock','frame size'
        );
        $rxRequiredList = array('rx frame cost', 'frame price');
        $stockRequiredList = array(
            'cost','color code','lens color','gradient','mirror','polarized','price'
        );

        $priceList = array('price','frame price','cost', 'rx frame cost');

        $yesNoList = Array(
            'gradient','mirror','polarized','folding','rx service','bestseller','stock','new product'
        );
        $booleanList = Array('folding','bestseller');
        $numbersList = array('upc','frame size','temple length','bridge size', 'lens height', 'lens width', 'quantity','position');
        if(strtoupper($this->data['rx service']) == 'YES'){
            $missingRequired = '';
            foreach ($rxRequiredList as $required){
                if($this->data[$required] == ''){
                    $missingRequired .= $required . " ";
                }
                if($missingRequired != ''){
                    exit($this->data['model code'] . " must have a value for these attributes: $missingRequired");
                }
            }
        }
        if(strtoupper($this->data['stock']) == 'YES'){
            $missingRequired = '';
            foreach ($stockRequiredList as $required){
                if($this->data[$required] == ''){
                    $missingRequired .= $required . " ";
                }
                if($missingRequired != ''){
                    exit($this->data['model code'] . " must have a value for these attributes: $missingRequired");
                }
            }
        }
        if(strtoupper($this->data['stock']) == 'NO'){
            $priceList = array_diff( $priceList, ['cost', 'price'] );
            $yesNoList = array_diff ($yesNoList,['gradient','mirror','polarized']);
            $numbersList = array_diff($numbersList,['upc']);
        }
        foreach ($firstUppercase as $attr) {
            $this->data[$attr] = ucwords(strtolower($this->data[$attr]));
        }
        foreach($requiredList as $required){

            $missingRequired = '';
            if($this->data[$required] == ''){
                $missingRequired .= $required . " ";
            }
            if($missingRequired != ''){
                exit($this->data['model code'] . " must have a value for these attributes: $missingRequired");
            }
        }
        foreach($priceList as $price){
            //Thanks @dbr https://stackoverflow.com/questions/308122/simple-regular-expression-for-a-decimal-with-a-precision-of-2
            if (!preg_match("/^[0-9]+(\.[0-9]{1,2})?$/", $this->data[$price]) && $this->data[$price] != ''){
                exit( $this->data['model code'] . " must have price value for value of $price");
            }
        }
        foreach($yesNoList as  $yesNo){
            $this->data[$yesNo] = strtoupper($this->data[$yesNo]);
            switch(true){
                case !in_array($this->data[$yesNo], array('YES','NO','')):
                    exit( $this->data['model code'] .
                        " must have either yes or no for value of $yesNo");
                    break;
            }
        }
        foreach($booleanList as $boolean){
            $this->data[$boolean] = 1;
            if ($this->data[$boolean] == 'NO') {
                $this->data[$boolean] = 0;
            }
        }
        foreach($numbersList as $number){
            if (!preg_match('/^[0-9]+(\.[0-9])?$/', $this->data[$number]) && $this->data[$number] != ''){
                exit( $this->data['model code'] . " must have numbers for value of $number");
            }
        }
    }
}