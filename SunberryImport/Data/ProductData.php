<?php
abstract class ProductData
{
    public $data;
    public $hasRx;
    public $isStock;

    function __construct($data)
    {
        $data['gender'] = ucwords($data['gender']);
        $this->data = $data;
        $this->hasRx = strtolower($data['rx service']) == 'yes';
        $this->isStock = strtolower($data['stock']) == 'yes';
        $this->data['sports specific'] = $this->checkSports($this->data['sports specific']);
        $this->data['prescription range'] = str_replace(' to ', '/',$this->data['prescription range']);
        $this->data['progressive range'] = str_replace(' to ', '/',$this->data['progressive range']);
        $this->data['gender'] = str_replace('Men', 'Man',$this->data['gender']);
        $this->data['gender'] = str_replace('Women', 'Woman',$this->data['gender']);
    }

    public function checkSports($sports){
        new Magento();
        $sportsCategories = explode(',' ,$sports);
        $upperCaseSports = array();
        if(!empty($sportsCategories)){
            $attributeCode =  'ts_sports';
            $attributeOptions = Mage::getSingleton('eav/config')
                ->getAttribute('catalog_product', $attributeCode)
                ->getSource()
                ->getAllOptions();
            $optionLabels = array_column($attributeOptions,'label');

            foreach ($sportsCategories as $sport){
                $sport = trim(ucwords($sport));
                $upperCaseSports[] = $sport;
                if(!array_search($sport,$optionLabels) && $sport){
                    exit(
                        'Please supply one or more of the following values for sports specific : ' .
                        implode(' ', $optionLabels)
                    );
                }

            }
        }
        return implode (',',$upperCaseSports);
    }
}

