<?php
/**
 * Extends ProductData
 * Inherits the following properties from ProductData
 * $data;
 * $hasRx;
 * $isStock;
 * $firstTime;
 */
require_once( 'ProductData.php' );


class OakleyData extends ProductData
{

    function __construct($data){
        parent::__construct ($data);
        $this->checkData();
    }

    function checkData()
    {   $firstUppercase = array('shape', 'gender', 'frame type','front material', 'lens material');
        $requiredList =array(
            'frame color','gender','brand','status','category code','rx service','stock','frame size',
            'model name description','frame price'
        );
        $genderValues = array('Woman', 'Man', 'Unisex');
        if($this->data['category code'] == 'OX' || $this->data['category code'] == 'OY' || $this->data['rx service'] == 'NO'){
                unset($requiredList[array_search('frame price', $requiredList)]);
        }

        $stockRequiredList = array(
            'color code','lens color','prizm','transition','iridium', 'polarized','price','cost'
        );
        $rxRequiredList = array('rx frame cost');
        $priceList = array('price','cost','frame price', 'rx frame cost');

        $yesNoList = Array(
            'prizm','transition','iridium','polarized','folding','rx service','bestseller','new product','stock'
        );
        $booleanList = Array('folding','bestseller');
        $numbersList = array('upc','frame size','temple length','bridge size', 'lens height', 'lens width', 'quantity','sort order');
        if(strtoupper($this->data['stock']) == 'NO'){
            $priceList = array_diff( $priceList, ['cost', 'price'] );
            $yesNoList = array_diff ($yesNoList,['prizm','transition','iridium','polarized']);
            $numbersList = array_diff($numbersList,['upc']);
        }
        if(strtoupper($this->data['rx service']) == 'YES'){
            $missingRequired = '';
            foreach ($rxRequiredList as $required){
                if($this->data[$required] == ''){
                    $missingRequired .= $required . " ";
                }
                if($missingRequired != ''){
                    exit($this->data['model name description'] . " must have a value for these attributes: $missingRequired");
                }
            }
        }
        if(strtoupper($this->data['stock']) == 'YES'){
            $missingRequired = '';
            foreach ($stockRequiredList as $required){
                if($this->data[$required] == ''){
                    $missingRequired .= $required . " ";
                }
                if($missingRequired != ''){
                    exit($this->data['model name description'] . " must have a value for these attributes: $missingRequired");
                }
            }
        }
        foreach ($firstUppercase as $attr) {
            $this->data[$attr] = ucwords(strtolower($this->data[$attr]));
        }

        if(!in_array($this->data['gender'],$genderValues)){
            echo $this->data['gender'];
            exit ('Gender  must be one of the following values' . implode($genderValues));
        }
        foreach($requiredList as $required){

            $missingRequired = '';
            if($this->data[$required] == ''){
                $missingRequired .= $required . " ";
            }
            if($missingRequired != ''){
                exit($this->data['model name description'] . " must have a value for these attributes: $missingRequired");
            }
        }
        foreach($priceList as $price){
            //Thanks @dbr https://stackoverflow.com/questions/308122/simple-regular-expression-for-a-decimal-with-a-precision-of-2
            if (!preg_match("/^[0-9]+(\.[0-9]{1,2})?$/", $this->data[$price]) && $this->data[$price] != ''){
                exit( $this->data['model name description'] . " must have price value for value of $price");
            }
        }
        foreach($yesNoList as  $yesNo){
            $this->data[$yesNo] = strtoupper($this->data[$yesNo]);
            switch(true){
                case !in_array($this->data[$yesNo], array('YES','NO','')):
                    exit( $this->data['model name description'] .
                        " must have either yes or no for value of $yesNo");
                    break;
            }
        }
        foreach($booleanList as $boolean){
            $this->data[$boolean] = 1;
            if ($this->data[$boolean] == 'NO') {
                $this->data[$boolean] = 0;
            }
        }
        foreach($numbersList as $number){
            if (!preg_match('/^[0-9]+(\.[0-9])?$/', $this->data[$number]) && $this->data[$number] != ''){
                exit( $this->data['model name description'] . " must have numbers for value of $number");
            }
        }
        $oakleyCategories = array(
            'M E N - L I F E S T Y L E','M E N - S P O R T','MEN ACTIVE','MEN ICONIC','MEN LIFESTYLE',
            'MEN SPORT',"MEN'S ACTIVE",'MEN-ACTIVE','MEN-ICONIC','MEN-LIFESTYLE','MEN-SPORT','New Product',
            'Oakley Bestseller','W O M A N - L I F E S T Y L E','WOMEN ACTIVE','Y O U T H - S U N','YOUTH',
            "M E N ' S - S P O R T", 'Y O U T H S U N'
        );
        $badCategories = '';
        foreach (explode(',',$this->data['oakley category']) as $cat) {
            if(!in_array($cat, $oakleyCategories) && $cat != ''){
                $badCategories .= ',' .$cat;
            }
        }
        if($badCategories != ''){
            exit('The following are not valid values for the oakley category column ' . $badCategories);
        }

        $this->data['oakley category'] = $this->cleanOakleyCategories($this->data['oakley category']);
    }

    private function cleanOakleyCategories($oakleyCategory){
        $correctValues = array(
            'MEN-ACTIVE' => 'MEN ACTIVE',
            "MEN'S ACTIVE" => 'MEN ACTIVE',
            'MEN-ICONIC' =>  'MEN ICONIC',
            'M E N - L I F E S T Y L E' => 'MEN LIFESTYLE',
            'MEN-LIFESTYLE' => 'MEN LIFESTYLE',
            'MEN-SPORT' => 'MEN SPORT',
            "M E N ' S - S P O R T"  => 'MEN SPORT',
            'M E N - S P O R T' => 'MEN SPORT',
            'W O M A N - L I F E S T Y L E' => 'WOMAN LIFESTYLE',
            'Y O U T H - S U N' => 'YOUTH SUN',
            'Y O U T H S U N' => 'YOUTH SUN'
        );

        foreach (explode(',',$oakleyCategory) as $cat) {
            if(key_exists($cat, $correctValues)){
                $oakleyCategory = str_replace($cat, $correctValues[$cat], $oakleyCategory);
            }
        }
        return $oakleyCategory;
    }
}

