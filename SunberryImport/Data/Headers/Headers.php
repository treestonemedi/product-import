<?php
abstract class Headers
{
    public $headers = array();

    function __construct($data)
    {
        DataHelper::checkHeaders(current(current($data))[0], $this->headers);
    }
}