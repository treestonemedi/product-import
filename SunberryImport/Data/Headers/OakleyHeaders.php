<?php
 class OakleyHeaders extends Headers
{
     //Here are the correct headers for an Oakley Csv
     public $headers = Array(
         'upc','description','short description','status','frame color','frame size','price',
         'cost','rx frame cost','color code','lens color','lens base','lens material','temple length','bridge size',
         'lens height','prizm','transition','iridium','polarized','folding','rx service','geofit', 'shape', 'frame type',
         'temple material','front material','bestseller','oakley category','new product', 'gender','theme', 'brand',
         'category code', 'prescription range','progressive range','model name description','lens width','stock',
         'frame price','configurable sku','quantity','sort order', 'minimum single vision pd', 'cylinder',
         'sports specific','model number','max cylinder'
     );
}
