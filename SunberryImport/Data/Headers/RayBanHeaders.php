<?php
class RayBanHeaders extends Headers
{
    //Here are the correct headers for an Ray Ban Csv
    public $headers = array(
        'upc','model code','configurable sku','model name description','description','price','cost','rx frame cost','brand',
        'rx service','stock','frame color','color code','lens color','frame size','temple length','bridge size',
        'lens width','lens height','lens base','lens material','temple material','front material','folding',
        'shape','frame type','theme','gradient','mirror','polarized','bestseller','gender','prescription range',
        'progressive range','position','status','quantity','frame price','sports specific','model number',
        'max cylinder'
    );
}